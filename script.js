$(document).ready(function() {

    const API_KEY = '42d6e4b8371349dead68d83643cb08be';
    const INTERVAL = '1day';
    const SYMBOLS = ['ADBE', 'AAPL', 'BIDU', 'BKNG', 'EBAY', 'FB', 'EA', 'INTC', 'MSFT', 'NFLX', 'NVDA', 'PYPL', 'QCOM', 'MRNA', 'SBUX', 'TSLA', 'ZM', 'GOOG', 'GEVO', 'WIMI', 'NIO', 'XPEV', 'MA', 'XOM', 'AMD', 'BABA', 'AAL', 'KO', 'PEP', 'LMT', 'AXP', 'T', 'ALV', 'BA', 'CVX', 'DAL', 'GE', 'GM', 'MRO', 'MGM', 'NOK', 'NCLH', 'ORCL', 'TWTR', 'DIS', 'WDC', 'PLTR', 'TRIP', 'IBM', 'EXPE', 'ATVI', 'CERN', 'CSCO', 'CMCSA', 'DISCA', 'JD', 'LBTYK', 'MCHP', 'STX', 'TXN', 'VOD', 'WDC', 'XLNX', 'BK', 'BKR', 'CCL', 'C', 'DVN', 'DOW', 'F', 'FCX', 'GPS', 'GIS', 'HST', 'HPQ', 'KEY', 'KR', 'M', 'MRO', 'MU', 'MS', 'MOS', 'NEM', 'NEE', 'OXY', 'PFE', 'PSX', 'RCL', 'SLB', 'WFC', 'VIAC', 'V', 'VZ', 'FTI', 'YQ', 'AMTX', 'AFMD', 'ABNB', 'AESE', 'AMRN', 'POWW', 'ASTC', 'ARVL', 'APHA', 'BLU', 'BXRX', 'BLDP', 'BNGO', 'BIOL', 'CELC', 'CAN', 'CSCW', 'CIDM', 'CRIS', 'CRON', 'WISH', 'DKNG', 'ENDP'];
    const GROUP_LENGTH = 8;
    const SLEEP_TIME = 61000;
    const RSI_MAX = 30;

    let symbols_groups = [];
    let current_index = 0;
    let stocks = [];
    let stocks_rsi = [];
    let fetch_interval;
    let progress_bar = document.querySelector('#progress-bar');
    let btn_load = document.querySelector('#btn-load');
    let ul_symbol = document.querySelector('#symbol');
    let ul_rsi = document.querySelector('#rsi');

    for (let i = 0; i < Math.floor(SYMBOLS.length / GROUP_LENGTH) + 1; i++) {
        symbols_groups.push(SYMBOLS.slice(i * GROUP_LENGTH, (i + 1) * GROUP_LENGTH));
    }

    function api_url(i) {
        let str = 'https://api.twelvedata.com/rsi?symbol=';
        str += symbols_groups[i].join(',') + '&interval=' + INTERVAL + '&apikey=' + API_KEY;
        console.log("api_url:   API URL built   -->   " + str);
        return str
    }

    function fetchData() {
        axios.get(api_url(current_index)).then(resp => {
            console.log("fetchData:   Data fetched for:   " + symbols_groups[current_index].join(", "));
            saveData(resp.data);
            analyzeData();
            updatePage();
            console.log("fetchData:   Updating progress bar...");
            let width = (current_index + 1) * GROUP_LENGTH * 100 / SYMBOLS.length;
            if (width >= 100) {
                width = 100;
                progress_bar.classList.add('bg-success');
            }
            progress_bar.style.width = width + '%';
            current_index++;
            if (current_index == symbols_groups.length) {
                current_index = 0;
                btn_load.disabled = false;
                clearInterval(fetch_interval);
                console.log("fetchData:   All data retrieved. Clearing interval.");
            }
        }).catch(error => {
            console.log(error);
        });
    }

    btn_load.addEventListener('click', function() {
        console.log("btnClicked:   Button clicked. Starting fetcher...");
        ul_symbol.innerHTML = "";
        ul_rsi.innerHTML = "";
        progress_bar.classList.remove('bg-success');
        btn_load.disabled = true;
        fetchData();
        fetch_interval = setInterval(fetchData, SLEEP_TIME);
    });

    function saveData(resp_data) {
        if (!('status' in resp_data)) {
            Object.keys(resp_data).forEach(key => {
                stocks.push(resp_data[key]);
            });
        } else {
            stocks.push(resp_data);
        }
        console.log("saveData:   Data saved");
    }

    function updatePage() {
        ul_symbol.innerHTML = "";
        ul_rsi.innerHTML = "";
        symbol_HTML = "";
        rsi_HTML = "";
        stocks_rsi.forEach(stock_rsi => {
            if (stock_rsi.rsi < RSI_MAX) {
                symbol_HTML += `<li><a href="${stock_rsi.url}">${stock_rsi.symbol}</a></li>`;
                rsi_HTML += `<li class="bg-success">${stock_rsi.rsi}</li>`;
            } else {
                symbol_HTML += `<li><a href="${stock_rsi.url}">${stock_rsi.symbol}</a></li>`;
                rsi_HTML += `<li>${stock_rsi.rsi}</li>`;
            }
        });
        ul_symbol.innerHTML = symbol_HTML;
        ul_rsi.innerHTML = rsi_HTML;
        console.log("updatePage:   Lists updated");
    }

    function analyzeData() {
        rsi();
        console.log("analyzeData:   Data analyzed");
    }

    function rsi() {
        for (let i = current_index * GROUP_LENGTH; i < stocks.length; i++) {
            let stock = stocks[i];
            if (stock.status == 'ok') {
                let stock_rsi = {
                    symbol: stock.meta.symbol,
                    url: `https://it.investing.com/search/?q=${stock.meta.symbol}`,
                    rsi: Number(stock.values[0].rsi)
                }
                insertStockRSI(stock_rsi);
            } else {
                console.log("rsi:   Stock status != ok, discarding...");
                console.log(stock);
            }
        };
    }

    function insertStockRSI(stock_rsi) {
        if (stocks_rsi.length < 1 || stocks_rsi[stocks_rsi.length - 1].rsi < stock_rsi.rsi) {
            stocks_rsi.push(stock_rsi);
            return;
        }
        for (let i = 0; i < stocks_rsi.length; i++) {
            if (stocks_rsi[i].rsi > stock_rsi.rsi) {
                stocks_rsi.splice(i, 0, stock_rsi);
                return;
            }
        }
    }

});